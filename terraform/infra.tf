provider "docker" {
  host = "ssh://ansible@can.cloudns.asia:587"
}

resource "docker_image" "centos" {
  name = "centos:latest"
}

resource "docker_container" "slat-master" {
  image = "${docker_image.centos.latest}"
  name = "slat-master"
  command =  ["tail", "-f", "/dev/null"]
  labels {
      label="env"
      value = "var.env"
      
  }
  
}

resource "docker_container" "minion1" {
  image = "${docker_image.centos.latest}"
  name = "minion1"
  command =  ["tail", "-f", "/dev/null"]
  labels {
      label="env"
      value = "var.env"
  }
}

resource "docker_container" "minion2" {
  image = "${docker_image.centos.latest}"
  name = "minion2"
  command =  ["tail", "-f", "/dev/null"]
  labels {
      label="env"
      value = "var.env"
  }
}
